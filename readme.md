Welcome to Blazor!
==================

This is a demo project playground for experimenting with clientside Blazor.

[Link to Blazor introduction](https://docs.microsoft.com/en-us/aspnet/core/blazor/?view=aspnetcore-3.0)